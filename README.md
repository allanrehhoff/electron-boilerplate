#Electron App Boilerplate Setup#

1. If npm isn't available yet, you must first install node.js, for the operating system that you're currently using.  
[Download node.js](https://nodejs.org/en/download/)  

2. Open a command prompt/terminal and install dependencies/build tools.  
`cd your/app/dir/`
`npm install`

	2.1 If you get any WARN's related to ajv, you probably just need to update or install the package.  
		This can be fixed by running:
		`npm install ajv`  

3. Make any required build/information changes to package.json.  
Refer to the [electron-builder configuration manual](https://github.com/electron-userland/electron-builder/tree/master/docs/configuration) for details.  

4. While developing your app, you can run and test it by running:  
`electron .`  
or using the wrapper command provided  
`npm run start`

5. Build your app.  
The default configurations builds your app for the 3 major platforms, windows, MACOS, and linux (debian). 
`npm run build`  